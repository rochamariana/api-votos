import express, { Express, Request, Response, NextFunction } from 'express';
import cors from 'cors';

// DB Votos
const VoteModel = require('./models/Vote');
require('./db');

const app: Express = express();

app.use(express.json());
app.use(cors());

app.get('/votes', async (req: Request, res: Response, next: NextFunction) => {
  res.json(await VoteModel.findAll());
  console.log('Get ok')
});

app.post('/vote', async (req: Request, res: Response, next: NextFunction) => {

    const votacao = await VoteModel.create(req.body);
    console.log('Post ok')
    res.json(votacao);
});

app.listen(3000);