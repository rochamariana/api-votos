import { DataTypes, Model } from 'sequelize';
const db = require('../db');

class Vote extends Model { }

Vote.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    candidate: {
        type: DataTypes.STRING(45),
        allowNull: false
    },
    role: {
        type: DataTypes.STRING(45),
        allowNull: false
    },
    party: {
        type: DataTypes.STRING(45),
        allowNull: false
    }
}, {
    sequelize: db,
    tableName: 'vote',
    modelName: 'Vote',
    createdAt: 'criacao',
    updatedAt: 'atualizacao',
    timestamps: true
});

module.exports = Vote;